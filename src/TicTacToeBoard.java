
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.LineBorder;

/**
 * 
 * @author veronikatuul Veronika Tuul tegi (ideid ammutasin kõrgeaulislt
 *         Google'lt) IA-17 kõik kommentaarid, mis koodis on mu enda jaoks -
 *         õppimiseks
 */

public class TicTacToeBoard extends JApplet {
	public static void main(String[] args) {

		JFrame holder = new JFrame("TicTacToe"); // raam, mis "hoiab" mängu

		TicTacToeBoard applet = new TicTacToeBoard();

		holder.add(applet, BorderLayout.CENTER);

		holder.setSize(300, 300); // raami kujundus
		holder.setVisible(true); // et raam näha oleks :)
	}

	public TicTacToeBoard() {
		JPanel panel = new JPanel(new GridLayout(3, 3));
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				panel.add(cells[i][j] = new Box());

		add(panel, BorderLayout.CENTER);
		panel.setBorder(new LineBorder(Color.BLACK, 2));

		add(rank, BorderLayout.NORTH);
		rank.setBorder(new LineBorder(Color.BLACK, 2));

	}

	private char whoPlays = 'X';

	private Box[][] cells = new Box[3][3];
	// loob nö infotabloo, kes võitis, kelle kord jne
	private JLabel rank = new JLabel("Alusta mängu!");

	public boolean boardCompl() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (cells[i][j].getSymbol() == ' ')
					return false;

		return true;
	}

	// selle tegemiseks kasutasin ühe teise mängu näidist:
	public boolean won(char symbol) {
		for (int i = 0; i < 3; i++)
			if ((cells[i][0].getSymbol() == symbol) && (cells[i][1].getSymbol() == symbol)
					&& (cells[i][2].getSymbol() == symbol)) {
				return true;
			}

		for (int j = 0; j < 3; j++)
			if ((cells[0][j].getSymbol() == symbol) && (cells[1][j].getSymbol() == symbol)
					&& (cells[2][j].getSymbol() == symbol)) {
				return true;
			}

		if ((cells[0][0].getSymbol() == symbol) && (cells[1][1].getSymbol() == symbol)
				&& (cells[2][2].getSymbol() == symbol)) {
			return true;
		}

		if ((cells[0][2].getSymbol() == symbol) && (cells[1][1].getSymbol() == symbol)
				&& (cells[2][0].getSymbol() == symbol)) {
			return true;
		}

		return false;
	}

	public class Box extends JPanel {

		private char symbol = ' ';

		public Box() {
			setBorder(new LineBorder(Color.black, 1)); // lahtrite äärised
			setBackground(Color.WHITE);
			// lahtritele hiire "kuular"
			addMouseListener(new MyMouseListener());

		}

		public char getSymbol() {
			return symbol;
		}

		public void setSymbol(char c) {
			symbol = c;
			repaint();
		}

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			if (symbol == 'X') { // joonistab risti
				g.drawLine(10, 10, getWidth() - 10, getHeight() - 10);
				g.drawLine(getWidth() - 10, 10, 10, getHeight() - 10);
			} else if (symbol == 'O') {
				// joonistab ringi
				g.drawOval(5, 5, getWidth() - 10, getHeight() - 10);

			}
		}

		private class MyMouseListener extends MouseAdapter {

			public void mouseClicked(MouseEvent e) {
				// kui osa lahtrid on tühjad, võitjat veel ei ole " ... kord"
				if (symbol == ' ' && whoPlays != ' ') {
					setSymbol(whoPlays);

					// kui osa lahtrid on täis, ja on võitja, siis on "... on
					// võitja"

					if (won(whoPlays)) {
						rank.setText(whoPlays + " võitis");
						whoPlays = ' '; // mäng on läbi
						// kui kõik lahtrid on täis, kuid võitjat ei ole, on
						// "Viik"

					} else if (boardCompl()) {

						rank.setText("Viik");
						whoPlays = ' ';
					} else {
						// vahetab mängijat
						whoPlays = (whoPlays == 'X') ? 'O' : 'X';
						// näitab, kelle kord on
						rank.setText(whoPlays + " kord");
					}
				}
			}
		}
	}

}