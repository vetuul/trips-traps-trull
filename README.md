See on vana hea trips-traps-trull mäng, mille tegin IT Kolledži Programmeerimise alused aine raames kodutööna. 

Alljärgnev tekst on pigem endale õppimiseks, kui traditsiooniline ReadMe-tekst

Mängulaud on klass, mis on JAppleti laiendus, st, et selles klassis saab kasutada kõiki JAppleti konstruktsiooni osasid
Selles klassis on kõik mänguga seotud elemendid.

Meetod - TicTacToeBoard, mis:
hoiab raami
koondab kõik lahtrid - Box

Mängu juhtimine:
* kui kõik lahtrid on tühjad - kuvatakse rank JLable "Alusta mängu"
* kui osa lahtrid on tühjad, võitjat veel ei ole " ... kord"
* kui kõik lahtrid on täis, võitjat ei ole "Viik"
* kui osa lahtrid on täis, on võitja "... on võitja"


Võit on siis kui:
(Massiiv) vertikaalselt, horisontaalselt, diagonaalselt 3 ühesugust.